from constants import PLAYER1_TOKEN, CONFIG
import random, copy

class Player:
    def __init__(self, board=None, player_token=None, ):
        self.player_token = player_token
        self.board = board
    
    def get_next_move(self, board=None, player_token=None):
        board = board or self.board
        player_token = player_token or self.player_token
        board.print()
        
        while True:
            allowed_moves = board.allowed_moves
            if len(allowed_moves) == 0:
                return -1
            position = int(input('Player ' + CONFIG['players'][player_token]['name']+ ': '))
            position -= 1
            move = [move for move in allowed_moves if move[1] == position]
            if len(move) == 1:
                break
            print('Column {} is already full'.format(position + 1))
        return move[0]

class RandomPlayer(Player):

    def get_next_move(self, board=None, player_token=None):
        board = board or self.board
        player_token = player_token or self.player_token

        allowed_moves = board.allowed_moves
        if len(allowed_moves) > 0:
            rindex = random.randint(0, len(allowed_moves) - 1)
            return allowed_moves[rindex]
        # return False

class TrainedPlayer(Player):
    def __init__(self, board=None, player_token=None, model=None, randomize=0):
        super().__init__(board, player_token)
        self.model = model
        self.randomize = randomize

    def get_next_move(self, board=None, player_token=None, model=None, randomize=None):
        board = board or self.board
        player_token = player_token or self.player_token
        model = model or self.model
        if randomize is None:
            randomize = self.randomize

        evaluated_allowed_moves = [{'move': move, 'value': 0} for move in board.allowed_moves]
        for evaluated_allowed_move in evaluated_allowed_moves:
            board_copy = copy.deepcopy(board.board)
            board_copy[evaluated_allowed_move['move'][0]][evaluated_allowed_move['move'][1]] = player_token
            evaluated_allowed_move['value'] = model.predict(board_copy, CONFIG['players'][player_token]['prediction_index'])
        evaluated_allowed_moves.sort(key=lambda x: x['value'], reverse=True)
        index = 0
        if randomize:
            if randomize >=  len(board.allowed_moves):
                if len(board.allowed_moves) > 1:
                    index = random.randint(0, len(board.allowed_moves) - 1)
            else:
                index = random.randint(0, randomize-1)
        try:
            result= evaluated_allowed_moves[index]['move']
        except:
            print(evaluated_allowed_moves, index)
            raise
        return result