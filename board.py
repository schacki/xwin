import copy
from constants import (
    PLAYER1_TOKEN,
    PLAYER2_TOKEN,
    FIELD_EMPTY,

    GAME_STATE_READY_TO_START,
    GAME_STATE_RUNNING,
    GAME_STATE_FINISHED,

    RESULT_WINNER_PLAYER_1,
    RESULT_WINNER_PLAYER_2,
    RESULT_DRAW,

    CONFIG
)
from exceptions import (
    GameAlreadyFinished,
    WrongPlayer,
    ColumnFull
)

class Board:
    def __init__(
        self,
        height=7,
        width=7,
        x=4,        
    ):
        self.height = height
        self.width = width
        self.x = x
        self.state = GAME_STATE_READY_TO_START
        self.result = None
        self.board_history = []
        self.board = []
        self.allowed_moves = []
        self.reset_board()


    def reset_board(self):
        self.board = [[FIELD_EMPTY for w in range(self.width)] for h in range(self.height)]
        self.state = GAME_STATE_READY_TO_START
        self.result = None
        self.allowed_moves = self.get_allowed_moves()
        self.reset_board_history()


    def reset_board_history(self):
        self.board_history = []


    def add_to_board_history(self):
        self.board_history.append(copy.deepcopy(self.board))


    def display(self):
        board_string = '\n'
        for line in self.board:
            line_string = '| '
            for column in line:
                line_string += CONFIG['players'][column]['display'] + ' | '
            print(line_string)
            board_string += line_string + '\n'
        board_string += '|' + ''.join(['----|' for column in range(self.width)]) + '\n'
        board_string += '|' + ''.join(['  ' + str(column + 1) + ' |' for column in range(self.width)]) + '\n'
        board_string += '\n'
        board_string += '\n'
        return board_string


    def print(self):
        print(self.display())
        # return
        # for line in self.board:
        #     line_string = '| '
        #     for column in line:
        #         line_string += CONFIG['players'][column]['display'] + ' | '
        #     print(line_string)
        # print('|' + ''.join(['----|' for column in range(self.width)]))
        # print('|' + ''.join(['  ' + str(column + 1) + ' |' for column in range(self.width)]))
        # print('')


    def get_allowed_moves(self):
        allowed_columns = self.width * [None]
        for row_number, row in enumerate(self.board):
            for column_number, value in enumerate(row):
                if value == 0:
                    allowed_columns[column_number] = [row_number, column_number]
        allowed_moves = [move for move in allowed_columns if bool(move)]
        return allowed_moves


    def move(self, player_token, move):
        if self.state == GAME_STATE_READY_TO_START:
            self.state = GAME_STATE_RUNNING
        elif self.state == GAME_STATE_FINISHED:
            raise GameAlreadyFinished()
        if player_token not in [PLAYER1_TOKEN, PLAYER2_TOKEN]:
            raise WrongPlayer(player_token)
        if move not in self.allowed_moves:
            raise ColumnFull(move[1])
        
        self.board[move[0]][move[1]] = player_token
        self.add_to_board_history()
        
        result = self.check_win(player_token, move)
        self.allowed_moves = self.get_allowed_moves()
        if result == RESULT_WINNER_PLAYER_1:
            self.state = GAME_STATE_FINISHED
            self.result = result
        elif result == RESULT_WINNER_PLAYER_2:
            self.state = GAME_STATE_FINISHED
            self.result = result
        elif len(self.allowed_moves) == 0:
            self.state = GAME_STATE_FINISHED
            self.result = RESULT_DRAW
        

        return result


    def check_win(self, player_token, move):
        # left right
        row_number, column_number = move
        hits = 1
        if column_number < self.width:
            for column in range(column_number + 1, self.width):
                if self.board[row_number][column] == player_token:
                    hits += 1
                else:
                    break
        if column_number > 0:
            columns = list(range(0, column_number))
            columns.reverse()
            for column in columns:
                if self.board[row_number][column] == player_token:
                    hits += 1
                else:
                    break
        if hits >= self.x:
            if player_token == PLAYER1_TOKEN:
                return RESULT_WINNER_PLAYER_1
            elif player_token == PLAYER2_TOKEN:
                return RESULT_WINNER_PLAYER_2
            else:
                raise Exception('Error')

        # top down
        hits = 1
        if row_number < self.height:
            for row in range(row_number + 1, self.height):
                if self.board[row][column_number] == player_token:
                    hits += 1
                else:
                    break
        if row_number > 0:
            rows = list(range(0, row_number))
            rows.reverse()
            for row in rows:
                if self.board[row][column_number] == player_token:
                    hits += 1
                else:
                    break
        if hits >= self.x:
            if player_token == PLAYER1_TOKEN:
                return RESULT_WINNER_PLAYER_1
            elif player_token == PLAYER2_TOKEN:
                return RESULT_WINNER_PLAYER_2
            else:
                raise Exception('Error')

        # left up > right down
        hits = 1
        if row_number > 0 and column_number > 0:
            columns = list(range(0, column_number))
            columns.reverse()
            rows = list(range(0, row_number))
            rows.reverse()
            for row, column in zip(
                rows,
                columns
            ):
                if self.board[row][column] == player_token:
                    hits += 1
                else:
                    break
        if row_number < self.height and column_number < self.width:
            for row, column in zip(
                range(row_number +1, self.height),
                range(column_number + 1, self.width)
            ):
                if self.board[row][column] == player_token:
                    hits += 1
                else:
                    break
        if hits >= self.x:
            if player_token == PLAYER1_TOKEN:
                return RESULT_WINNER_PLAYER_1
            elif player_token == PLAYER2_TOKEN:
                return RESULT_WINNER_PLAYER_2
            else:
                raise Exception('Error')

        # left down > right up
        hits = 1
        if row_number < self.height and column_number > 0:
            columns = list(range(0, column_number))
            columns.reverse()
            rows = list(range(row_number +1, self.height))
            for row, column in zip(
                rows,
                columns
            ):
                if self.board[row][column] == player_token:
                    hits += 1
                else:
                    break
        if row_number > 0 and column_number < self.width:
            columns = list(range(column_number + 1, self.width))
            rows = list(range(0, row_number))
            rows.reverse()
            for row, column in zip(
                rows,
                columns
            ):
                if self.board[row][column] == player_token:
                    hits += 1
                else:
                    break
        if hits >= self.x:
            if player_token == PLAYER1_TOKEN:
                return RESULT_WINNER_PLAYER_1
            elif player_token == PLAYER2_TOKEN:
                return RESULT_WINNER_PLAYER_2
            else:
                raise Exception('Error')

        return None

