import json

from constants import (
    PLAYER1_TOKEN,
    PLAYER2_TOKEN,
    GAME_STATE_FINISHED,
    CONFIG
)


class Game:
    def __init__(self, board=None, player1=None, player2=None, verbosity=1, interactive=0):
        self.board = board
        self.player1 = player1
        self.player2 = player2
        self.verbosity = verbosity
        self.interactive = interactive


    def start(self, verbosity=None, interactive=None):
        def do_move(player, player_token):
            move = player.get_next_move(
                self.board,
                player_token
            )
            if not move:
                raise Exception('Move required')
            self.board.move(player_token, move)
            return move

        if verbosity is None:
            verbosity = self.verbosity
        if interactive is None:
            interactive = self.interactive

        stop = False
        move = None
        player_token = None

        while not stop:
            for player, player_token in [[self.player1, PLAYER1_TOKEN], [self.player2, PLAYER2_TOKEN]]:
                try:
                    move = do_move(player, player_token)
                except:
                    self.board.print()
                    raise
                if self.board.state == GAME_STATE_FINISHED:
                    # if verbosity >= 1:
                    #     print('Final Move: ', CONFIG['players'][player_token]['name'], move[0]+1, move[1]+1)
                    stop = True
                    break
            if verbosity > 1:
                self.board.print()
            if interactive > 1:
                input('Press key to continue.')

        if verbosity >= 1:
            print('Final Move: ', CONFIG['players'][player_token]['name'], move[0]+1, move[1]+1)
            self.print()
        if verbosity == 1:
            self.board.print()
        if interactive == 1:
            input('Press key to continue.')

        return self.board.result

    def reset(self):
        self.board.reset_board()


    def print(self):
        if self.board.state == GAME_STATE_FINISHED:
            print(CONFIG['results'][self.board.result])
        else:
            print(CONFIG['states'][self.board.state])


    def collect_training_data(self):
        training_data = []
        for board in self.board.board_history:
            training_data.append((self.board.result, board))
        return training_data


