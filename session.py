import json
from constants import (
    RESULT_DRAW,
    RESULT_WINNER_PLAYER_1,
    RESULT_WINNER_PLAYER_2,
    CONFIG
)

class Session:
    def __init__(self, game, rounds=1, collect_training_data=False):
        self.game = game
        self.rounds = rounds
        self.collect_training_data = collect_training_data
        self.stats = {
            RESULT_DRAW: 0,
            RESULT_WINNER_PLAYER_1: 0,
            RESULT_WINNER_PLAYER_2: 0,
        }
        self.training_data = []


    def start(self, rounds=None, collect_training_data=None):
        if rounds is None:
            rounds = self.rounds
        if collect_training_data is None:
            collect_training_data = self.collect_training_data

        for i in range(rounds):
            result = self.game.start()

            if self.game.board.result == RESULT_DRAW:
                self.stats[RESULT_DRAW] += 1
            elif self.game.board.result == RESULT_WINNER_PLAYER_1:
                self.stats[RESULT_WINNER_PLAYER_1] += 1
            elif self.game.board.result == RESULT_WINNER_PLAYER_2:
                self.stats[RESULT_WINNER_PLAYER_2] += 1

            if collect_training_data:
                self.training_data.extend(self.game.collect_training_data())
            self.game.reset()
        
        return self.stats


    def reset(self):
        self.training_data = []
        self.stats = {
            RESULT_DRAW: 0,
            RESULT_WINNER_PLAYER_1: 0,
            RESULT_WINNER_PLAYER_2: 0,
        }
        self.game.reset()


    def display(self):
        return ('{}: {:3} | {}: {:3} | {}: {:3}'.format(
            CONFIG['results'][RESULT_WINNER_PLAYER_1],
            self.stats[RESULT_WINNER_PLAYER_1],
            CONFIG['results'][RESULT_WINNER_PLAYER_2],
            self.stats[RESULT_WINNER_PLAYER_2],
            CONFIG['results'][RESULT_DRAW],
            self.stats[RESULT_DRAW]
        ))
    
    
    def print(self):
        print (self.display())


    def save(self):
        data_to_json = json.dumps(self.training_data)
        with open("data/training_data.json", "w") as json_file:
            json_file.write(data_to_json)


    def load(self):
        json_file = open('data/training_data.json', 'r')
        self.training_data = json.loads(json_file.read())