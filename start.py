import os, logging

os.environ['TF_CPP_MIN_VLOG_LEVEL'] = '3'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['AUTOGRAPH_VERBOSITY'] = '3'
logging.getLogger('tensorflow').disabled = True
logging.getLogger("tensorflow").setLevel(logging.ERROR)
logging.getLogger("tensorflow").addHandler(logging.NullHandler(logging.ERROR))

logging.getLogger('scipy').disabled = True
logging.getLogger('scipy').setLevel(logging.ERROR)
logging.getLogger('scipy').addHandler(logging.NullHandler(logging.ERROR))

import tensorflow as tf
tf.get_logger().setLevel('ERROR')
tf.autograph.set_verbosity(3)

from constants import RESULT_WINNER_PLAYER_2
from model import XWinModel
from player import Player, RandomPlayer, TrainedPlayer
from game import Game
from board import Board
from session import Session

# playing field height
HEIGHT = 5

# playing field width
WIDTH = 7

# number of connected coins to win
X = 4

# number of epochs to train the model
EPOCHS = 100

# batch size to train the model
BATCH_SIZE = 100

# percentage of data that should be used for training
TRAINING_DATA_PERCENTAGE = 0.8


# Modes
print('1) Human vs Human')
print('2) Human vs Random')
print('3) Model training with self-learning: random vs random')
print('4) Model training with self-learning: incremental trained vs incremental trained')
choice = input('Your selection?  ')


if choice == '1':
    # Create the playing field
    board = Board(height=HEIGHT, width=WIDTH, x=X)

    # Create a human player (can be used for player 1 and 2)
    player_human = Player()

    # Create the game and start it
    game = Game(board, player_human, player_human, verbosity=1)
    game.start()


elif choice == '2':
    # Create the playing field
    board = Board(height=HEIGHT, width=WIDTH, x=X)

    # Create a human player
    player_human = Player()

    # Create a random player, aka randomly selecting a move
    player_random = RandomPlayer()

    # Create the game and start it
    game = Game(board, player_human, player_random, verbosity=1)
    game.start()


elif choice == '3':
    # How many rounds should be played to generate training data?
    rounds = int(input('Rounds?  '))

    # Create the playing field
    board = Board(height=HEIGHT, width=WIDTH, x=X)

    # Create a human player
    player_human = Player()

    # Create a random player, aka randomly selecting a move
    player_random = RandomPlayer()

    # Create trained player and model which is used by the trained player 
    # to predict the best next moves
    model = XWinModel(
        number_of_inputs=board.width*board.height,
        number_of_outputs=3,
        width=board.width,
        height=board.height,
        x=board.x,
        epochs=EPOCHS,
        batch_size=BATCH_SIZE,
        training_data_percentage=TRAINING_DATA_PERCENTAGE
    )
    player_trained = TrainedPlayer(model=model)

    # Create the game
    game = Game(board, player_random, player_random, verbosity=0)

    # Create a session the specified numer of games=rounds
    session = Session(game, rounds=rounds, collect_training_data=True)
    session.start()
    print('Traing: ', session.display())

    # Train the model with the session training data
    model.train(session.training_data, verbose=0)
    model.print()

    # Let the trained player play against the random player
    game = Game(board, player_trained, player_random, verbosity=0)
    session = Session(game, rounds=100)
    session.start()
    print('Test: ', session.display())

    # Let the trained player play against a human player
    game = Game(board, player_trained, player_human, verbosity=1)
    while input('Play(y)').lower() == 'y':
        game.start()
        game.reset()


elif choice == '4':
    # How many rounds should be played per session to generate training data?
    rounds = int(input('Rounds?  '))

    # How many sessions should be played?
    iterations = int(input('Iterations?  '))

    # Create the playing field
    board = Board(height=HEIGHT, width=WIDTH, x=X)

    # Create a human player
    player_human = Player()

    # Create a random player, aka randomly selecting a move
    player_random = RandomPlayer()

    # Create trained player and model which is used by the trained player 
    # to predict the best next moves
    # To collect training data, we add the randomize flag so that 1 out of the best
    # 3 moves are picked to get variance in the training data
    model = XWinModel(
        number_of_inputs=board.width*board.height,
        number_of_outputs=3,
        width=board.width,
        height=board.height,
        x=board.x,
        epochs=EPOCHS,
        batch_size=BATCH_SIZE,
        training_data_percentage=TRAINING_DATA_PERCENTAGE
    )
    player_trained = TrainedPlayer(model=model, randomize=3)

    # Create the game with trained player against trained player
    game = Game(board, player_trained, player_trained, verbosity=0)

    # Create a session the specified numer of games=rounds
    session = Session(game, rounds=rounds, collect_training_data=True)

    total_result = {
        'wins': [],
        'loss': [],
        'accuracy': []
    }

    # Each iteration is a sequence of play and model training, so that the
    # next iteration starts with improved trained players.
    for iteration in range(iterations):
        result = session.start()
        total_result['wins'].append(int(result[RESULT_WINNER_PLAYER_2]/rounds*100))
        history = model.train(session.training_data, verbose=0)
        total_result['loss'].extend([history.history['loss'][0], history.history['loss'][-1]])
        total_result['accuracy'].extend([history.history['accuracy'][0], history.history['accuracy'][-1]])
        session.reset()
        print(total_result['wins'])
    model.print()

    # Reset randomize to zero again. We want the best trained player now.
    player_trained.randomize = 0

    # Let the trained player play against the random player
    game = Game(board, player_trained, player_random, verbosity=0)
    session = Session(game, rounds=rounds)
    session.start()
    print('Test: ', session.display())

    # Let the trained player play against a human player
    game = Game(board, player_trained, player_human, verbosity=2)
    while input('Play(y)').lower() == 'y':
        game.start()
        game.reset()

