PLAYER1_TOKEN = -1
PLAYER2_TOKEN = 1

FIELD_EMPTY = 0

GAME_STATE_READY_TO_START = 'A'
GAME_STATE_RUNNING = 'B'
GAME_STATE_FINISHED = 'C'

RESULT_WINNER_PLAYER_1 = PLAYER1_TOKEN
RESULT_WINNER_PLAYER_2 = PLAYER2_TOKEN
RESULT_DRAW = 0

CONFIG = {
    'players': {
        PLAYER1_TOKEN: {
            'name': 'Player 1',
            'display': ' 1',
            'prediction_index': 2
        },
        PLAYER2_TOKEN: {
            'name': 'Player 2',
            'display': ' 2',
            'prediction_index': 0
        },
        FIELD_EMPTY: {
            'display': ' o',
            'prediction_index': 1
        }
    },
    'results': {
        RESULT_WINNER_PLAYER_1: 'Winner player 1',
        RESULT_WINNER_PLAYER_2: 'Winner player 2',
        RESULT_DRAW: 'Draw'
    },
    'states': {
        GAME_STATE_READY_TO_START: 'Ready to start',
        GAME_STATE_RUNNING: 'Running',
        GAME_STATE_FINISHED: 'Over'
    },
}