from keras.layers import Dense
from keras.layers import Dropout
from keras.models import Sequential
from keras.utils import to_categorical
from keras.models import model_from_json
import numpy as np
import matplotlib.pyplot as plt

class XWinModel:

    def __init__(self, number_of_inputs, number_of_outputs, width, height, x, epochs, batch_size, training_data_percentage):
        self.epochs = epochs
        self.batch_size = batch_size
        self.number_of_inputs = number_of_inputs
        self.number_of_outputs = number_of_outputs
        self.x = x
        self.width = width
        self.height = height
        self.training_data_percentage = training_data_percentage
        self.model = None
        self.reset()

    def reset(self):
        self.model = Sequential()
        # self.model.add(Dense(self.x *self.number_of_inputs, activation='relu', input_shape=(self.number_of_inputs,)))
        # self.model.add(Dense(self.x *self.number_of_inputs, activation='relu'))
        # self.model.add(Dense(self.number_of_outputs, activation='softmax'))
        # self.model.compile(loss='categorical_crossentropy', optimizer="rmsprop", metrics=['accuracy'])

        # self.model.add(Dense(self.x *self.number_of_inputs, activation='relu', input_shape=(self.number_of_inputs,)))
        # self.model.add(Dense(self.number_of_inputs, activation='relu'))
        # self.model.add(Dense(self.x, activation='relu'))
        # self.model.add(Dense(self.number_of_outputs, activation='softmax'))
        # self.model.compile(loss='categorical_crossentropy', optimizer="rmsprop", metrics=['accuracy'])

        self.model.add(Dense(self.x * self.width * self.number_of_inputs, activation='relu', input_shape=(self.number_of_inputs,)))
        self.model.add(Dense(self.x * self.number_of_inputs, activation='relu'))
        self.model.add(Dense(self.number_of_inputs, activation='relu'))
        self.model.add(Dense(self.x, activation='relu'))
        self.model.add(Dense(self.number_of_outputs, activation='softmax'))
        self.model.compile(loss='categorical_crossentropy', optimizer="rmsprop", metrics=['accuracy'])

    def train(self, dataset, verbose=0):
        input = []
        output = []
        for data in dataset:
            input.append(data[1])
            output.append(data[0])

        X = np.array(input).reshape((-1, self.number_of_inputs))
        y = to_categorical(output, num_classes=self.number_of_outputs)

        boundary = int(self.training_data_percentage * len(X))
        X_train = X[:boundary]
        X_test = X[boundary:]
        y_train = y[:boundary]
        y_test = y[boundary:]
        self.history = self.model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=self.epochs, batch_size=self.batch_size, verbose=verbose)
        return self.history

    def predict(self, data, index):
        prediction = self.model.predict(np.array(data).reshape(-1, self.number_of_inputs))
        return prediction[0][index]

    def save(self):
        model_json = self.model.to_json()
        with open("data/model.json", "w") as json_file:
            json_file.write(model_json)
        self.model.save_weights("data/model.h5")
    

    def load(self):  
        json_file = open('data/model.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)
        self.model.load_weights("data/model.h5")

    def print(self):
        # summarize history for accuracy
        plt.plot(self.history.history['accuracy'])
        plt.plot(self.history.history['val_accuracy'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()

        # summarize history for loss
        plt.plot(self.history.history['loss'])                                                      
        plt.plot(self.history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()
