class ColumnFull(Exception):
    def __init__(self, column):
        super().__init__('Column {} is already full.'.format(column))

class WrongPlayer(Exception):
    def __init__(self, player):
        super().__init__('It is not player\'s  {} turn.'.format(player))

class GameAlreadyFinished(Exception):
    def __init__(self, player):
        super().__init__('Game already finished')